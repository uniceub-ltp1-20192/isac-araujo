#2.1 Crie um programa que defina duas variáveis, quantidade_cebolas
#e preco_cebola, atribua valores inteiros para essas variáveis.
#Imprima o valor de quantidade_cebolas muliplicado pelo
#preco_cebola, da seguinte forma:
#“Você irá pagar” + resultado da multiplicação + “pelas cebolas.”

quantidade_cebolas = 7
preco_cebola       = 5
calcular_cebolas   = (quantidade_cebolas)*(preco_cebola)
print('Você irá pagar',calcular_cebolas, 'reais pelas cebolas.')

#2.2 Faça um comentário em português em cada linha do programa
#abaixo explicando para você mesmo que elas fazem. Execute o
#programa abaixo, corrigindo os erros.

#Variaveis que armazenam números inteiros
cebolas = 300
cebolas_na_caixa = 120
espaco_caixa = 5
caixas = 60
#Subtração de váriaveis para obtém quantas cebolas estão fora da caixa
cebolas_fora_da_caixa = cebolas-cebolas_na_caixa
#Divisão do espaço do caixa com o cebolas que estão na caixa menos o total de caixas
#para obter o valor de caixas vazias
caixas_vazias = caixas-(cebolas_na_caixa/espaco_caixa)
#Divisão de para encontrar o numero de caixas necessárias
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
#Todo print é feito entre parenteses
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos,", caixas_vazias, "caixas vazias")
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")

#2.3 Brinque com os valores das variáveis do programa 2.2 e veja o
#que acontece com a saída do seu programa.

cebolas = 3.55
cebolas_na_caixa = 1450.1
espaco_caixa = 5.85
caixas = 78.8

cebolas_fora_da_caixa = cebolas-cebolas_na_caixa
caixas_vazias = caixas-(cebolas_na_caixa/espaco_caixa)
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos,", caixas_vazias, "caixas vazias")
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")