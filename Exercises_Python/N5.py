#5.1 Crie um programa que solicite um valor ao usuário e
#informe se o mesmo é par ou impar.

number  = float(input('Digite um número para saber se é par ou impar:'))
result = number % 2

if result == 0:
    print('Número é par')
else:
    print('Número é impar')

#5.2 Conceba uma programa que informe se um determinado
#número é primo.

num=int(input("Digite um número: "))
if num%2==1:
    print(num,"é primo!")
else:
    print(num,"não é primo!")

#5.3 Crie uma calculadora automática que dado dois valores
#informe o resultado da adição, subtração, multiplicação e
#divisão.

value1 = float(input("Informe o primeiro número: "))
value2 = float(input("Informe o segundo número: "))

soma=value1+value2
sub =value1-value2
mult=value1*value2
div =value1/value2
print("\n\tA soma desses números é ",soma)
print("\tA subtração desses números é ",sub)
print("\tA multiplicação desses números é ",mult)
print("\tA divisão desses números é ",div)
